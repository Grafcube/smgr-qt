import winreg


def set_bluetooth(disabled=True):
    """
    Set Bluetooth access with a registry key.
    """

    key_path = r"SOFTWARE\Microsoft\PolicyManager\default\Connectivity\AllowBluetooth"
    value_name = "value"
    value_data = 0 if disabled else 2

    try:
        reg_key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, key_path, 0,
                                 winreg.KEY_WRITE)
        winreg.SetValueEx(reg_key, value_name, 0, winreg.REG_DWORD, value_data)
        winreg.CloseKey(reg_key)
        print("Bluetooth has been disabled."
              if disabled else "Bluetooth has been enabled.")
    except Exception as e:
        print("An error occurred:", e)


def set_cmd(disabled=True):
    """
    Set CMD access with registry key. GPO is only available on Pro editions.
    """

    key_path = r"Software\Policies\Microsoft\Windows\System"
    value_name = "DisableCMD"
    value_data = 1 if disabled else 2

    try:
        reg_key = winreg.CreateKey(winreg.HKEY_CURRENT_USER, key_path)
        winreg.SetValueEx(reg_key, value_name, 0, winreg.REG_DWORD, value_data)
        winreg.CloseKey(reg_key)
        print(
            "CMD has been disabled." if disabled else "CMD has been enabled.")
    except Exception as e:
        print("An error occurred:", e)


def set_usb(disabled=True):
    """
    Set USB access with registry key.
    """

    key_path = r"SYSTEM\CurrentControlSet\Services\USBSTOR"
    value_name = "Start"
    value_data = 4 if disabled else 3

    try:
        reg_key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, key_path, 0,
                                 winreg.KEY_WRITE)
        winreg.SetValueEx(reg_key, value_name, 0, winreg.REG_DWORD, value_data)
        winreg.CloseKey(reg_key)
        print(
            "USB has been disabled." if disabled else "USB has been enabled.")
    except Exception as e:
        print("An error occurred:", e)


def set_domain_access(domain, restrict=True):
    """
    Edit hosts file to redirect `domain` to localhost. Note that most browsers
    (such as Edge) use DNS over HTTPS by default, which may bypass this restriction.
    Windows Firewall can be used to block sites but it only works for IP addresses
    and dynamic DNS makes this infeasible.
    """

    hosts_path = r"C:\Windows\System32\drivers\etc\hosts"
    entry = "127.0.0.1 " + domain

    try:
        content = ""
        with open(hosts_path, 'r') as hosts:
            content = hosts.read()
        if restrict:
            with open(hosts_path, 'a+') as hosts:
                if entry in content:
                    print(f"`{domain}` is already blocked.")
                else:
                    hosts.write('\n' + entry)
                    print(f"`{domain}` has been blocked.")
        else:
            new = content.replace('\n' + entry, '')
            with open(hosts_path, 'w+') as hosts:
                hosts.write(new)
            if new == content:
                print(f"`{domain}` is not blocked.")
            else:
                print(f"`{domain}` has been unblocked.")
    except Exception as e:
        print("An error occurred:", e)
