import gui
import pyuac
from win32api import CloseHandle, GetCurrentProcess
from win32con import (EWX_REBOOT, TOKEN_ADJUST_PRIVILEGES, TOKEN_QUERY)
from win32security import (AdjustTokenPrivileges, LookupPrivilegeValue,
                           OpenProcessToken, SE_PRIVILEGE_ENABLED,
                           SE_SHUTDOWN_NAME)


def main():
    """
    Obtains SE_SHUTDOWN_NAME privilege required for restart API and creates GUI.
    """

    privilege_id = LookupPrivilegeValue(None, SE_SHUTDOWN_NAME)
    process_handle = GetCurrentProcess()
    process_token = OpenProcessToken(process_handle,
                                     TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY)
    if process_token is None:
        print("Could not get process token.")
        return
    AdjustTokenPrivileges(process_token, False,
                          ((privilege_id, SE_PRIVILEGE_ENABLED), ))
    CloseHandle(process_token)

    # Create QT6 GUI
    gui.create_window()


if __name__ == "__main__":
    # Obtain Administrator priviliges before starting
    if not pyuac.isUserAdmin():
        print("Re-launching as admin.")
        pyuac.runAsAdmin()
    else:
        main()
