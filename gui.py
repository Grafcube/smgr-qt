import sys
import win32api
from utils import *
from functools import partial
from PyQt6.QtWidgets import (QMainWindow, QSplitter, QVBoxLayout, QHBoxLayout,
                             QWidget, QApplication, QPushButton)


class State:

    def __init__(self):
        self.bluetooth = True
        self.cmd = True
        self.usb = True
        self.domain = True


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self.state = State()
        self.setWindowTitle("Security Manager")
        self.main_layout = QVBoxLayout()
        central_widget = QWidget(self)
        central_widget.setLayout(self.main_layout)
        self.setCentralWidget(central_widget)
        self.config_button("Bluetooth access", "bluetooth")
        self.config_button("CMD access", "cmd")
        self.config_button("USB access", "usb")
        self.config_button("Domain access", "domain")
        self.main_layout.addWidget(QSplitter())
        self.main_layout.addWidget(QSplitter())
        self.confirm = QHBoxLayout()
        self.main_layout.addLayout(self.confirm)
        self.apply_button()
        self.restart_button()
        self.show()

    def config_button(self, name, state):
        button = QPushButton(
            name + (" (ON)" if getattr(self.state, state) else " (OFF)"))
        self.main_layout.addWidget(button)
        button.clicked.connect(partial(self.toggle_config, button, name,
                                       state))

    def toggle_config(self, button, name, state):
        setattr(self.state, state, not getattr(self.state, state))
        button.setText(name +
                       (" (ON)" if getattr(self.state, state) else " (OFF)"))

    def apply_button(self):
        button = QPushButton("Apply")
        self.confirm.addWidget(button)
        button.clicked.connect(self.apply_settings)

    def apply_settings(self):
        set_bluetooth(not self.state.bluetooth)
        set_cmd(not self.state.cmd)
        set_usb(not self.state.usb)
        set_domain_access("facebook.com", not self.state.domain)

    def restart_button(self):
        button = QPushButton("Restart")
        self.confirm.addWidget(button)
        button.clicked.connect(self.restart_system)

    def restart_system(self):
        win32api.InitiateSystemShutdown(
            None, "Your device will restart in 5 seconds", 5, 1, 0)


def create_window():
    app = QApplication(sys.argv)
    app.setApplicationName("com.gitlab.Grafcube.SecurityManager")
    window = MainWindow()
    window.show()
    sys.exit(app.exec())
